# job-openshift

Esempio di come deployare su openshift un JOB e relativi cronjob
Con lo stesso deployment, attraverso la creazione di piu cronjob, è possibile lanciare un batch in un POD in maniera parametrica
Sia attraverso il passaggio diretto di parametri al jar (vedi file cronjob-001.yaml e cronjob-002.yaml)
Sia attraverso la creazione di configmap con parametri diversi, da iniettare sempre nei cronjob (vedi file cronjob-001.yaml e cronjob-002.yaml)