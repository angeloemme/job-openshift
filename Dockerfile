FROM openjdk:8-jdk
ADD job-openshift-1.0.jar job-openshift-1.0.jar
CMD ["java", "-jar", "job-openshift-1.0.jar"]
