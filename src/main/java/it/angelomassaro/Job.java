package it.angelomassaro;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class Job {

	private static String NUM_SECONDI = ((System.getenv("NUM_SECONDI")!=null) ? System.getenv("NUM_SECONDI") : "10");
	
	public static void main(String[] args) {
		Job job = new Job();
		job.fase1(args);
	}
	
	private void fase1(String[] args) {
		System.out.println("DENTRO FASE 1 - INIZIO!!!");
		System.out.println("ENV VAR NUM_SECONDI: " + System.getenv("NUM_SECONDI") );
		System.out.println("Argomenti passati come parametro: " + Arrays.toString(args));
		
		for (int i = 1; i <= Integer.parseInt(NUM_SECONDI); i++) {
			try {
				System.out.println("SONO FERMO AL SECONDO: " + i + " - Primo Parametro: " + (args != null && args.length > 0 ? args[0] : "") );
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		
		System.out.println("DENTRO FASE 1 - FINE !!!");
	}

}
